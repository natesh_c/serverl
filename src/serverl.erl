%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 09. Mar 2016 11:37 AM
%%%-------------------------------------------------------------------
-module(serverl).
-author("tringapps").

-behaviour(gen_server).

%% API
-export([spwan_one_more/1,start_link/0]).

%% gen_server callbacks
-export([init/1,handle_call/3,handle_cast/2,handle_info/2,terminate/2,code_change/3,format_status/2]).

-export([create_http/3, create_websocket/3]).

-record(state, {running_ports=[]}).

%%%===================================================================
%%% API
%%%===================================================================

start_link() -> gen_server:start_link({local,?MODULE},?MODULE,[],[]).

create_http(Port,CallbackModule,Args) -> gen_server:call(?MODULE,{http,Port,CallbackModule,Args}).

create_websocket(Port,CallbackModule,Args) -> gen_server:call(?MODULE,{http_ws,Port,CallbackModule,Args}).

spwan_one_more(ListenSocket)when is_port(ListenSocket) -> gen_server:cast(?MODULE,{one_more,ListenSocket}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->  {ok, #state{}}.

handle_call({https,Port,_CallbackModule,_Arg}=Args,_From, State) ->  handle_listen_port_response(listen_port(ssl,Port),Args,State);

handle_call({http,Port,_CallbackModule,_Arg}=Args,_From, State) ->  handle_listen_port_response(listen_port(gen_tcp,Port),Args,State);

handle_call({https_ws,Port,_CallbackModule,_Arg}=Args,_From, State) ->  handle_listen_port_response(listen_port(ssl,Port),Args,State);

handle_call({http_ws,Port,_CallbackModule,_Arg}=Args,_From, State) ->  handle_listen_port_response(listen_port(gen_tcp,Port),Args,State);

handle_call(_Request, _From, State) ->  {reply, ok, State}.

handle_cast({one_more,ListenSocket}, #state{running_ports = List} = State) ->
  create_acceptors(temp,1,lists:keyfind(ListenSocket,1,List)),
  {noreply, State};

handle_cast(_Request, State) ->  {noreply, State}.

handle_info(_Info, State) ->  {noreply, State}.

terminate(_Reason, _State) ->  ok.

code_change(_OldVsn, State, _Extra) ->  {ok, State}.

format_status(_,_) -> ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================

listen_port(Mod,Port) -> Mod:listen(Port,[binary,{reuseaddr, true},{active, false}]).

handle_listen_port_response({ok,ListenSocket},Args,#state{running_ports = List} =State) ->
  create_acceptors(permanent,5,{ListenSocket,Args}),

  {reply,success,State#state{running_ports = [{ListenSocket,Args}|List]}};

handle_listen_port_response({error,Reason},_,State) -> {reply,{error,Reason},State}.


create_acceptors(_,_,false) -> ok;
create_acceptors(_,0,_Args) -> ok;
create_acceptors(temp,Count,Args) ->  handle_eprocess_response(monerl:create_temp_process(),Args),
  create_acceptors(temp,Count-1,Args);
create_acceptors(permanent,Count,Args) ->  handle_eprocess_response(monerl:create_permanent_process(),Args),
  create_acceptors(permanent,Count-1,Args).


handle_eprocess_response({ok,Pid},Args) ->  spwan_new_acceptor(Pid,Args);
handle_eprocess_response(not_allow,_Args) ->  ok;
handle_eprocess_response(_Any,_Args) ->  ok.


spwan_new_acceptor(Pid,{ListenSockt,{http,_Port,CallbackModule,Arg}}) ->  ehttp:tcp_setup({Pid,ListenSockt},CallbackModule,Arg,[]);
%% spwan_new_acceptor(Pid,{ListenSockt,{https,_Port,CallbackModule,Arg}}) ->  ehttp:tcp_setup({Pid,ListenSockt},CallbackModule,Arg,[]);
spwan_new_acceptor(Pid,{ListenSockt,{http_ws,_Port,CallbackModule,Arg}}) ->  ewebsocket:tcp_setup({Pid,ListenSockt},CallbackModule,Arg,[]).
%% spwan_new_acceptor(Pid,{ListenSockt,{https_ws,_Port,CallbackModule,Arg}}) ->  ehttp:tcp_setup({Pid,ListenSockt},CallbackModule,Arg,[]).


