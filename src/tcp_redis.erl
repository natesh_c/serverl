%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. May 2016 12:16 PM
%%%-------------------------------------------------------------------
-module(tcp_redis).
-author("tringapps").
-include_lib("rediserl/include/redis.hrl").
%% API
-export([endponit/2,hit_single/2,hit_multiple/2]).

-record(state,{host,port}).

endponit(Host,Port) -> #state{host = Host,port = Port}.

hit_single(#redis_data{} = RD,#state{} = State) ->
  case hit(rediserl:single_cmd(RD),State) of

    {ok,[Data]} -> conform_redis_response(redis_q(RD),Data);

    Rest -> io:format("Unhandle single resp ~p~n",[Rest])

  end.


hit_multiple(CMDs,#state{} = State) ->
  case hit(rediserl:multi_cmds(CMDs),State) of

    {ok,Items} -> conform_multi_redis(Items,[redis_q(CMd) || CMd <- CMDs],[]);

    Rest -> io:format("Rest ~p ~n",[Rest]),Rest
  end.

conform_multi_redis([],[],Result) -> lists:reverse(Result);
conform_multi_redis([{status,<<"OK">>}|Rest],Operations,Result) ->  conform_multi_redis(Rest,Operations,Result);
conform_multi_redis([{status,<<"QUEUED">>}|Rest],Operations,[{q,N}|Result]) ->  conform_multi_redis(Rest,Operations,[{q,N+1}|Result]);
conform_multi_redis([{status,<<"QUEUED">>}|Rest],Operations,Result) -> conform_multi_redis(Rest,Operations,[{q,1}|Result]);
conform_multi_redis([{list,Items}|Rest],Operations,[{q,N}|Result]) when length(Items) =:= N->
  {OP,ResultSet} = multi_loop(Items,Operations,[]),
  conform_multi_redis(Rest,OP,ResultSet ++ Result);
conform_multi_redis([Data|Rest],[Operation|RestOperation],Result) ->
  conform_multi_redis(Rest,RestOperation,[conform_redis_response(Operation,Data)|Result]);
conform_multi_redis([{list,[]}],[],[]) -> [].

redis_q(#redis_data{type = Type,operation = Operation} = _) -> {Type,Operation}.

multi_loop([],OP,Result) -> {OP,Result};
multi_loop([Item|Rest],[Op|OpRest],Result) -> multi_loop(Rest,OpRest,[conform_redis_response(Op,Item)|Result]).


conform_redis_response({#type.set,#set.add},{number,0}) -> false;
conform_redis_response({#type.set,#set.add},{number,_}) -> true;
conform_redis_response({#type.set,#set.member},{number,0}) -> false;
conform_redis_response({#type.set,#set.member},{number,1}) -> true;
conform_redis_response({#type.basic,#basic.exists},{number,0}) -> false;
conform_redis_response({#type.basic,#basic.exists},{number,1}) -> true;
conform_redis_response({#type.basic,#basic.exists},{number,N}) -> N;
conform_redis_response({#type.hash,#hash.save_all},{number,0}) -> false;
conform_redis_response({#type.hash,#hash.save_all},{number,_}) -> true;
conform_redis_response({#type.hash,#hash.collect_all},{list,Items}) -> form_key_pair(Items,[]);
conform_redis_response({#type.hash,#hash.collect_some},{list,Items}) -> remove_keys(Items,[]);
conform_redis_response({#type.set,#set.get_all},{list,Items}) -> remove_keys(Items,[]);
conform_redis_response(_,{list,Items}) -> Items;
conform_redis_response(_,{chars,String}) -> String;
conform_redis_response(_,{number, Integer}) -> Integer;
conform_redis_response(_,{status, Status}) -> {status, Status};
conform_redis_response(_,{error, Error}) -> {error, Error};
conform_redis_response(_,nil) -> nil;
conform_redis_response(_,Result) ->io:format("Unhandle redis cmd ~p~n",[Result]),ok.

hit(Data,#state{host = Host,port = Port}) ->
  tcp_client:hit(tcp_client:data(Data,tcp_client:redis(tcp_client:new_tcp(Host,Port)))).

form_key_pair([],Result) -> lists:reverse(Result);
form_key_pair([nil|T],L) -> form_key_pair(T,L);
form_key_pair([{_,Val}|T],[{Key}|L]) -> form_key_pair(T,[{Key,Val}|L]);
form_key_pair([{_,Key}|T],L) -> form_key_pair(T,[{Key}|L]).


remove_keys([],Result) -> lists:reverse(Result);
remove_keys([nil|Rest],Result) -> remove_keys(Rest,[nil|Result]);
remove_keys([{_,Val}|Rest],Result) -> remove_keys(Rest,[Val|Result]).


