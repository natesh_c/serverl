%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 06. Apr 2016 4:51 PM
%%%-------------------------------------------------------------------
-module(sample_websocket).
-author("tringapps").
-behaviour(ewebsocket).
%% API
-export([send/2]).
-export([init/1,on_handshake/2,on_connect/1,on_close/1,on_cast/2,on_response/2,on_request/2,on_sent/2]).

-record(state,{}).

%%%===================================================================
%%% APIs
%%%===================================================================

send(Pid,Message) -> ewebsocket:push(Pid,Message).

%%%===================================================================
%%% ewebsocket callbacks
%%%===================================================================

-spec init(Args::any()) -> {ok,State::any()}.
init(_Args) -> {ok,#state{}}.

-spec on_handshake(Action::binary(),State::any()) -> {allow,State::any() | {disallow,any()}}.
on_handshake(Action,State) -> io:format("handshake action ~p~n",[Action]), {allow,State}.

-spec on_connect(State::any()) -> {ok,State::any()}.
on_connect(State) -> io:format("Connection success~n"),{ok,State}.

-spec on_close(State::any()) -> {ok,State::any()}.
on_close(State) -> io:format("connection is closed ~n"),{ok,State}.

-spec on_cast(Content::term(),State::any()) -> {ok,State::any()}.
on_cast(_Content,State) -> {ok,State}.

-spec on_response(Data::binary(),State::any()) -> {ok,Reply::term(),State::any()}.
on_response(Data,State) -> io:format("received response ~p~n",[Data]),{ok,Data,State}.

-spec on_request(Data::binary(),State::any()) -> {ok,Data::binary(),State::any()} | {close,Data::binary(),State::any()}.
on_request(Data,State) -> io:format("received request ~p~n",[Data]),
  {ok,<<"OK">>,State}.

-spec on_sent(Status::atom(),State::any()) -> {ok,State::any()}.
on_sent(_Status,State) -> {ok,State}.

%%%===================================================================
%%% Internal methods
%%%===================================================================











%%%===================================================================
%%% Helper methods
%%%===================================================================




