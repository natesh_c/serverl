%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Mar 2016 11:28 AM
%%%-------------------------------------------------------------------
-module(tcp_server_process).
-author("tringapps").
-behaviour(instance_process_worker).

%% API
-export([setup/4,send_data/2]).
-export([init/1,do_before/1,do/2,do_after/2,handle_call/2,handle_cast/2,on_terminate/2,cast/2]).

-record(state,{args,listen_socket, call_back_module,cbm_state,socket,keep_live = false}).

%%%===================================================================
%%% callbacks
%%%===================================================================

-callback init(Arguments::term()) -> {ok,State::any()}.

-callback on_connect(State::any()) -> {ok,State::any()}.

-callback on_disconnect(State::any()) -> {ok,State::any()}.

-callback on_sent(Status::atom(),State::any()) -> {ok,State::any()}.

-callback on_cast(Data::term(),State::any()) -> {ok,State::any()}.

-callback on_request(RequestData::term(),State::any()) ->  {ok,ResponseData::binary(),State::any()}|{loop,ResponseData::binary(),State::any()}.

-callback on_response(RequestData::term(),State::any()) ->  {reply,Reply::any(),State::any()}.

-callback data_receiver(State::any()) -> http | ws |default.

%%%===================================================================
%%% API
%%%===================================================================

setup({Pid,LSocket},CallBackMod,Args,[]) ->  monerl:start_work(Pid,?MODULE,[{CallBackMod,Args,LSocket}]).

%% send_and_receive_data(Pid,BinData) -> monerl:make_call(Pid,{send_and_receive_data,BinData}).

send_data(Pid,Data) -> monerl:make_cast(Pid,{send_data,Data}).

%% cast_callback(Pid,Data,CallBackModule)-> monerl:make_cast_callback(Pid,{cast_callback,Data},CallBackModule).

cast(Pid,Message) -> monerl:make_cast(Pid,{cast,Message}).

%%%===================================================================
%%% process_worker callbacks
%%%===================================================================

-spec init(InitalArguments::any()) -> {ok,State::any()}.

init([{CallBackModule,Args,ListenSocket}]) ->
  {ok,State} = CallBackModule:init(Args),
  {ok,#state{call_back_module = CallBackModule,listen_socket = ListenSocket,cbm_state = State}}.

-spec do_before(State::term()) -> {retry,State::any()} | {continue,NextLevelArguments::any(),State::term()}
| {kill,Reason::any(),State::term()}.

do_before(#state{listen_socket = ListenSocket } = State) ->
  handle_gen_tcp_accept_response(gen_tcp:accept(ListenSocket,5000),State).

-spec do(Arguments::any(),State::term()) -> {retry,Arguments::any(),State::any()} |
    {continue,ResultSet::any(),State::term()} |
    {stop,Reason::any(),State::term()}.

do({ok,Socket},State) ->  handle_receiver_response(receiver({gen_tcp,Socket},State),{ok,Socket},State);
do(Reason,State) ->  io:format(" TCP receive is failed ~p~n",[Reason]),{stop,force_close,State}.

-spec do_after(Arguments::any(),State::term()) -> {retry,Arguments::any(),State::term()} | {next,State::term()}
| {next_do,Args::term(),State::term()} | {wait,Time::timeout(),State::term()} | {stop,Reason::any(),State::term()}.

do_after({ok,Data,DoArgs},#state{call_back_module = Mod,cbm_state = CBMState} =State) ->
  handle_on_request(Mod:on_request(Data,CBMState),DoArgs,State);

do_after(einval,State) -> {stop,<<"Error in Value">>,State};

do_after(_Arguments,State) -> {stop,<<"Error in Value">>,State}.

handle_call({send_and_receive_data,BinData},#state{socket = Socket}= State) ->
  handle_send_data(gen_tcp:send(Socket,BinData),State);

handle_call({cast_callback,BinData},#state{socket = Socket}= State) ->
  handle_send_data(gen_tcp:send(Socket,BinData),State);

handle_call(_Content,State) ->  {reply,unhandle,State}.


handle_cast({cast,Data},#state{call_back_module = Mod,cbm_state = ModState} = State) ->
  {ok,NewCState} = Mod:on_cast(Data,ModState),
  {noreply,State#state{cbm_state = NewCState}};

handle_cast({send_data,BinData},#state{socket = Socket}= State) ->  gen_tcp:send(Socket,BinData),
  {noreply,State};

handle_cast(_BinData,State) ->   {noreply,State}.

-spec on_terminate(Reason::any(),State::term()) -> ok.
on_terminate(_Reason,#state{call_back_module = Mod,cbm_state = CState}=State) ->
  {ok,NewCState} = Mod:on_disconnect(CState),
  {ok,State#state{cbm_state = NewCState}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_on_request({ok,Data,CState},Args,State) ->  {stop,normal,send_and_close_response({ok,Data},Args,State#state{cbm_state =CState})};
handle_on_request({loop,Data,CState},Args,State) -> {next_do,Args,send_response({ok,Data},Args,State#state{cbm_state =CState})}.

send_and_close_response({ok,BinData},{ok,Socket},#state{call_back_module = Mod,cbm_state = CBMState} = State) ->
  Res = gen_tcp:send(Socket,BinData),
  {ok,NewCState} = Mod:on_sent(Res,CBMState),
  gen_tcp:close(Socket),
  State#state{cbm_state = NewCState}.

send_response({ok,BinData},{ok,Socket},#state{call_back_module = Mod,cbm_state = CBMState} = State) ->
  Res = gen_tcp:send(Socket,BinData),
  {ok,NewCState} = Mod:on_sent(Res,CBMState),
  State#state{cbm_state =NewCState}.

handle_gen_tcp_accept_response({ok,Socket},#state{call_back_module = Mod,cbm_state = ModState,listen_socket = LSocket}= State) ->
  serverl:spwan_one_more(LSocket),
  {ok,NewModState} = Mod:on_connect(ModState),
  {continue,{ok,Socket},State#state{socket = Socket,cbm_state = NewModState}};

handle_gen_tcp_accept_response({error,timeout},State) -> {retry,State};
handle_gen_tcp_accept_response({error,closed},State) -> {stop,socket_closed,State};
handle_gen_tcp_accept_response({error,Reason},State) -> {stop,Reason,State}.

handle_receiver_response({ok,Data},Args,State) -> {continue,{ok,Data,Args},State};
handle_receiver_response({error,einval},_Args,State) -> {continue,einval,State};
handle_receiver_response({error,timeout},Args,#state{call_back_module = Mod,cbm_state = CState,socket = Socket}=State) ->
  case Mod:data_receiver(CState) of

    ws -> io:format("tcp server process retry ~n"),{retry,Args,State};

    _ -> gen_tcp:close(Socket),{stop,socket_closed,State#state{socket = unknown}}

  end;

handle_receiver_response({error,closed},_Args,State) -> {stop,socket_closed,State#state{socket = unknown}};
handle_receiver_response({error,Reason},_Args,State) -> {stop,Reason,State#state{socket = unknown}};
handle_receiver_response(Return,_Args,_State) -> throw({wrong_return,<<"handle_receiver_response">>,Return}).

handle_send_data(ok,#state{socket = Socket} = State) ->  receiver_response(receiver({gen_tcp,Socket},State),State);
handle_send_data({error,Reason},State) -> {reply,{error,Reason},State}.

receiver_response({ok,Data},#state{call_back_module = Mod,cbm_state = CState} = State) ->
  {reply,Reply,NewCState} = Mod:on_response(Data,CState),
  {reply,Reply,State#state{cbm_state = NewCState}};
receiver_response({error,einval},State) -> {reply,einval,State};
receiver_response({error,timeout},State) -> {reply,timeout,State};
receiver_response({error,closed},State) -> {stop,socket_closed,State#state{socket = unknown}};
receiver_response({error,Reason},State) -> {stop,Reason,State#state{socket = unknown}};
receiver_response(Return,_State) -> throw({wrong_return,<<"receiver_response">>,Return}).

receiver(Socket,#state{call_back_module = Mod,cbm_state = CState}=_State) ->  receiver(Socket,Mod:data_receiver(CState));
receiver(Socket,ws) -> socket_data_receiver:catch_data(ws,Socket);
receiver(Socket,DataType) -> socket_data_receiver:catch_data(DataType,Socket).


