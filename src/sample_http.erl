%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. Mar 2016 6:23 PM
%%%-------------------------------------------------------------------
-module(sample_http).
-author("tringapps").

-behaviour(ehttp).
-include_lib("parserl/include/http.hrl").

%% APIs


%% ehttp callback APIs
-export([init/1,on_connect/1,get/2,post/2,headers/3,json/3,xml/3,raw/3,request/2,return/2,return_headers/1,on_sent/2,on_disconnect/1]).

-record(state,{}).

%%%===================================================================
%%% ehttp callbacks
%%%===================================================================

-spec init(Args::any()) -> {ok,State::term()}.
init(_Args) ->  {ok,#state{}}.

-spec on_connect(State::any()) -> {ok,State::any()}.
on_connect(State) -> {ok,State}.

-spec get(Arguments::term(),State::any()) ->  {continue,Arguments1::term(),State::any()} | {return,#http_response_code{},Return::term(),State::any()}.
get({Action,_Argslist},State) -> {continue,[Action],State}.

-spec post(Action::binary(),State::any()) -> {continue,Arguments1::term(),State::any()} | {return,#http_response_code{},Return::term(),State::any()}.
post(_Action,State) -> {continue,[],State}.

-spec headers(Arguments1::term(),Headers::term(),State::any()) -> {continue,Arguments2::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.
headers(Action,Headers,State) -> {continue,Action++Headers,State}.

-spec json(Arguments2::term(),JsonData::term(),State::any()) -> {continue,Request::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.
json(_Args2,_Json,State) -> {continue,request,State}.

-spec xml(Arguments2::term(),JsonData::term(),State::any()) -> {continue,Request::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.
xml(_Args2,_Json,State) -> {continue,request,State}.

-spec raw(Arguments2::term(),JsonData::term(),State::any()) -> {continue,Request::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.
raw(_Args2,_Json,State) -> {continue,request,State}.

-spec request(Request::term(),State::any()) -> {return,#http_response_code{},Retrun::term(),State::any()}.
request(_Request,State) -> {return,#http_response_code.ok,[],State}.

-spec return(Return::term(),State::any()) -> {ok,#content_type{},content,State::any()}.
return(_Return,State) -> {ok,#content_type.text,<<"OK">>,State}.

-spec return_headers(State::any()) -> {ok,Headers::list(term()),State::any()}.
return_headers(State) -> {ok,[],State}.

-spec on_sent(Status::atom(),State::any()) -> {ok,NewState::any()}.
on_sent(_Status,State) -> {ok,State}.

-spec on_disconnect(State::any()) -> {ok,State::any()}.
on_disconnect(State) -> {ok,State}.

%%%===================================================================
%%% Internal Methods
%%%===================================================================








%%%===================================================================
%%% Helper Methods
%%%===================================================================









