%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. Apr 2016 7:03 PM
%%%-------------------------------------------------------------------
-module(tcp_client).
-author("tringapps").
-include_lib("rediserl/include/redis.hrl").
-include_lib("parserl/include/http.hrl").

%% API
-export([new_tcp/2,new_tcp_ssl/2,http/1,redis/1,time_out/2,data/2,hit/1]).

-record(req,{host,port,request_method= gen_tcp,response_type = http ,data = <<"">>,time_out = 5000}).

%%%===================================================================
%%%  API Function
%%%===================================================================

new_tcp(Host,Port) -> #req{request_method = gen_tcp,host = Host,port = Port}.

new_tcp_ssl(Host,Port) -> #req{request_method = ssl,host = Host,port = Port}.

http(#req{} = Req) -> Req#req{response_type = http}.

time_out(Time,#req{} = Req) -> Req#req{time_out = Time}.

data(#http_data{} = Data,#req{} = Req) -> {ok,Bin} = http_encoder:build(Data),Req#req{data = Bin};

data(Data,#req{} = Req) -> Req#req{data = Data}.

redis(#req{} = Req)-> Req#req{response_type = redis}.

hit(#req{host = Host,request_method = gen_tcp,port = Port,time_out = TimeOut} = Req) ->
  handle_response(gen_tcp:connect(Host,Port,[binary,{active , false}],TimeOut),Req);

hit(#req{host = Host,request_method = ssl,port = Port,time_out = TimeOut} = Req) ->
  handle_response(ssl:connect(Host,Port,[binary,{active , false}],TimeOut),Req).

%%%===================================================================
%%% Helper Functions
%%%===================================================================

handle_response({ok,Socket},#req{data = Data} = Req) ->  handle_send_response(gen_tcp:send(Socket,Data),Socket,Req);
handle_response({error,closed},_) -> {error,server_closed};
handle_response({error,timeout},_) -> {error,connection_timeout};
handle_response(_,_) -> {error,server_not_connected}.

handle_send_response(ok,Socket,#req{request_method = Method,response_type = Type} = Req) ->
  handle_data_receiver(socket_data_receiver:catch_data(Type,{Method,Socket}),Socket,Req);
handle_send_response({error,Reason},_Socket,#req{} = _) -> {error,Reason}.

handle_data_receiver({ok,Data},Socket,#req{request_method = gen_tcp} = _) -> gen_tcp:close(Socket),{ok,Data};
handle_data_receiver({ok,Data},Sockett,#req{request_method = ssl} =_) -> ssl:close(Sockett),{ok,Data};
handle_data_receiver({error,Reason},Socket,#req{request_method = gen_tcp} = _) -> gen_tcp:close(Socket),io:format("~p~n",[Reason]),{error,Reason};
handle_data_receiver({error,Reason},Sockett,#req{request_method = ssl} =_) -> ssl:close(Sockett),io:format("~p~n",[Reason]),{error,Reason}.

