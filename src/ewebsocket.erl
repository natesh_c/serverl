%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Mar 2016 2:33 PM
%%%-------------------------------------------------------------------
-module(ewebsocket).
-author("tringapps").
-behaviour(tcp_server_process).
-include_lib("parserl/include/websocket.hrl").
-compile([export_all]).
%% API
-export([tcp_setup/4,push/2,cast/2]).

%% tcp_server_process callback APIs
-export([init/1,on_connect/1,on_disconnect/1,on_sent/2,on_request/2,on_response/2,data_receiver/1]).

-record(state,{callbackmod,modstate,tcp_data_receiver}).

%%%===================================================================
%%% tcp_server_process callbacks
%%%===================================================================

-callback init(Args::any()) -> {ok,State::any()}.

-callback on_handshake(Action::binary(),State::any()) -> {allow,State::any() | {disallow,any()}}.

-callback on_connect(State::any()) -> {ok,State::any()}.

-callback on_close(State::any()) -> {ok,State::any()}.

-callback on_cast(Content::term(),State::any()) -> {ok,State::any()}.

-callback on_response(Data::binary(),State::any()) -> {ok,Reply::term(),State::any()}.

-callback on_request(Data::binary(),State::any()) -> {ok,Data::binary(),State::any()} | {close,Data::binary(),State::any()}.

-callback on_sent(Status::term(),State::any()) -> {ok,State::any()}.

-include_lib("parserl/include/http.hrl").

%%%===================================================================
%%% APIs Methods
%%%===================================================================

tcp_setup(Pid,CallBackMod,Args,_Option) -> tcp_server_process:setup(Pid,?MODULE,[{CallBackMod,Args}],[]).

push(Pid,#ws{} = Message) -> {ok,Bin} =websocket_encoder:build(Message), tcp_server_process:send_data(Pid,Bin).

cast(Pid,Content) -> tcp_server_process:cast(Pid,{ws_cast,Content}).

%%%===================================================================
%%% CallBack Methods
%%%===================================================================

-spec init(Arguments::term()) -> {ok,State::any()}.
init([{CallBackModule,Args}]) ->
  {ok,State} = CallBackModule:init(Args),
  {ok,#state{callbackmod = CallBackModule,modstate = State}}.

-spec on_connect(State::any()) -> {ok,State::any()}.
on_connect(State) -> {ok,State#state{tcp_data_receiver = http}}.

-spec on_disconnect(State::any()) -> {ok,State::any()}.
on_disconnect(#state{callbackmod = Mod,modstate = CState} = State) -> {ok,update_state(Mod:on_close(CState),State)}.

-spec on_sent(Status::atom(),State::any()) -> {ok,State::any()}.
on_sent(Status,#state{callbackmod = Mod,modstate = CState} = State) ->
  {ok,NewCState} = Mod:on_sent(Status,CState),
  {ok,State#state{modstate = NewCState}}.

-spec on_cast(Content::term(),State::any()) -> {ok,State::any()}.
on_cast({ws_cast,Content},#state{callbackmod = Mod,modstate = CState} = State) ->
  {ok,NewCState} = Mod:on_cast(Content,CState),
  {ok,State#state{modstate = NewCState}}.

-spec on_request(RequestData::term(),State::any()) ->  {ok,ResponseData::binary(),State::any()} | {loop,ResponseData::binary(),State::any()}.
on_request(#http_data{} =HD,State) ->  http(HD,State);
on_request(#ws{} = Data,State) ->  websocket(Data,State).

-spec on_response(RequestData::term(),State::any()) -> {reply,Reply::any(),State::any()}.
on_response(Data,State) -> ws_onresponse(Data,State);
on_response(_RequestData,State) -> {reply,unhandle,State}.

-spec data_receiver(State::any()) -> http | ws |default.
data_receiver(#state{tcp_data_receiver = Receiver} = _State) -> Receiver.

%%%===================================================================
%%% Http
%%%===================================================================

update_state({ok,CState},State) -> State#state{modstate = CState};
update_state(CState,State) -> State#state{modstate = CState}.

http_failure() -> {failure,http_encoder:build(http_data:set_code(#http_response_code.bad_request,http_data:new_response()))}.
http_success(Headers) -> {success,http_encoder:build(http_data:headers(Headers,http_data:set_code(#http_response_code.websocket_accepted,http_data:new_response())))}.

ws_accept_headers(Key) -> [{<<"Connection">>,<<"Upgrade">>},{<<"Upgrade">>,<<"websocket">>},{<<"Sec-WebSocket-Accept">>,Key}] .

-record(header,{connection,upgrade,key}).

http(#http_data{request =#method.get,action = Action,headers = Headers} = H,#state{callbackmod = Mod,modstate = CState}= State) ->
  io:format("Http ~p~n",[H]),
  handle_http(handshake_conformation(Mod:on_handshake(Action,CState),Headers,State)).

handle_http({{failure, {ok,Reply}},State}) -> {ok, Reply,State};
handle_http({{success, {ok,Reply}},#state{callbackmod = Mod,modstate = CState}=State}) ->
  io:format("Reply ~p~n",[Reply]),
  {loop, Reply,update_state(Mod:on_connect(CState),State#state{tcp_data_receiver = ws})}.

handshake_conformation({allow,CState},Headers,State) -> {verify_headers(http_headers(Headers,#header{})),State#state{modstate = CState}};
handshake_conformation({disallow,CState},_Headers,State) -> {http_failure(),State#state{modstate = CState}}.

verify_headers(#header{connection = false} = _) -> http_failure();
verify_headers(#header{upgrade = false} = _) -> http_failure();
verify_headers(#header{key = unknown} = _) -> http_failure();
verify_headers(#header{key = Key} = _) -> accept_websocket(binary_util:strip(Key)).

accept_websocket(Key) ->
  try
    http_success(ws_accept_headers(base64:encode(crypto:hash(sha,<<Key/binary,<<"258EAFA5-E914-47DA-95CA-C5AB0DC85B11">>/binary>>))))
  catch
      _:_  -> http_failure()
  end.

http_headers([],HState) -> HState;
http_headers([{<<"Connection">>,Conn}|Rest], HState)-> http_headers(Rest, HState#header{connection = conform_upgrade(Conn)});
http_headers([{<<"Upgrade">>,Upgrade}|Rest], HState) -> http_headers(Rest, HState#header{upgrade = conform_websocket(Upgrade)});
http_headers([{<<"Sec-WebSocket-Key">>,Key}|Rest], HState) -> http_headers(Rest, HState#header{key = binary_util:strip(Key)});
http_headers([_|Rest], HState) -> http_headers(Rest, HState).

conform_upgrade(Connection) -> lists:member(<<"Upgrade">>,binary_util:strip_list(binary_util:split(Connection,<<",">>))).

conform_websocket(Bin) -> websocket_tag(binary_util:strip(Bin)).

websocket_tag(<<"WebSocket">>) -> true;
websocket_tag(<<"webSocket">>) -> true;
websocket_tag(<<"websocket">>) -> true;
websocket_tag(_) -> false.

%%%===================================================================
%%% Websocket
%%%===================================================================

websocket(#ws{opcode = 8 }=_,#state{callbackmod=Mod,modstate=CState}=State) -> {ok,ws_close_data(),update_state(Mod:on_close(CState),State)};

websocket(Data,#state{callbackmod=Mod,modstate=CState}=State) ->  handle_websocket_data(Mod:on_request(Data,CState),State).

handle_websocket_data({ok,Bin,CState},State) -> {loop,ws_data(Bin),update_state(CState,State)};
handle_websocket_data({close,_Bin,CState},State) -> {ok,ws_close_data(),update_state(CState,State)}.

ws_data(Data) -> element(2,websocket_encoder:build(ws_data:add_data(ws_data:new(),Data))).

ws_close_data() -> element(2,websocket_encoder:build(ws_data:add_close_tag(ws_data:new()))).

ws_onresponse(Data,#state{callbackmod = Mod,modstate = CState} = State) ->  handle_on_response(Mod:on_response(Data,CState),State).

handle_on_response({ok,Reply,CState},State) -> {reply,Reply,update_state(CState,State)}.



