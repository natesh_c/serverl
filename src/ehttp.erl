%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Mar 2016 3:46 PM
%%%-------------------------------------------------------------------
-module(ehttp).
-author("tringapps").

-behaviour(tcp_server_process).
-include_lib("parserl/include/http.hrl").

%% tcp_server callbacks
-export([init/1,on_connect/1,on_disconnect/1,on_sent/2,on_cast/2,on_request/2,on_response/2,data_receiver/1]).

%% APIs
-export([tcp_setup/4]).

-record(state,{callbackmod,modstate}).

%%%===================================================================
%%% CallBack Methods
%%%===================================================================

-callback init(Args::any()) -> {ok,State::term()}.

-callback on_connect(State::any()) -> {ok,State::any()}.

-callback get(Action::term()|{term()},State::any()) ->  {continue,Arguments1::term(),State::any()} |{return,#http_response_code{},Return::term(),State::any()}.

-callback post(Action::binary(),State::any()) -> {continue,Arguments1::term(),State::any()} |{return,#http_response_code{},Return::term(),State::any()}.

-callback headers(Arguments1::term(),Headers::term(),State::any()) -> {continue,Arguments2::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.

-callback json(Arguments2::term(),JsonData::term(),State::any()) -> {continue,Request::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.

-callback xml(Arguments2::term(),JsonData::term(),State::any()) -> {continue,Request::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.

-callback raw(Arguments2::term(),JsonData::term(),State::any()) -> {continue,Request::term(),State::any()}|{return,#http_response_code{},Return::term(),State::any()}.

-callback request(Request::term(),State::any()) -> {return,#http_response_code{},Retrun::term(),State::any()}.

-callback return(Return::term(),State::any()) -> {ok,#content_type{},content,State::any()}.

-callback return_headers(State::any()) -> {ok,Headers::list(term()),State::any()}.

-callback on_sent(Status::atom(),State::any()) -> {ok,NewState::any()}.

-callback on_disconnect(State::any()) -> {ok,State::any()}.

%%%===================================================================
%%% API
%%%===================================================================

tcp_setup(Pid,CallBackMod,Args,_Option) -> tcp_server_process:setup(Pid,?MODULE,[{CallBackMod,Args}],[]).

%%%===================================================================
%%% tcp_server_process callbacks
%%%===================================================================

-spec init(Arguments::term()) -> {ok,State::any()} | ignore.
init([{CallBackModule,Args}]) ->
  {ok,State} = CallBackModule:init(Args),
  {ok,#state{callbackmod = CallBackModule,modstate = State}}.

-spec on_connect(State::any()) -> {ok,State::any()}.
on_connect(#state{callbackmod = Mod,modstate = CState} = State) ->
  {ok,NewCState} = Mod:on_connect(CState),
  {ok,State#state{modstate = NewCState}}.

-spec on_disconnect(State::any()) -> {ok,State::any()}.
on_disconnect(#state{callbackmod = Mod,modstate = CState} = State) ->
  {ok,NewCState} = Mod:on_disconnect(CState),
  {ok,State#state{modstate = NewCState}}.

-spec on_sent(Status::atom(),State::any()) -> {ok,State::any()}.
on_sent(Status,#state{callbackmod = Mod,modstate = CState} = State) ->
  {ok,NewCState} = Mod:on_sent(Status,CState),
  {ok,State#state{modstate = NewCState}}.

-spec on_cast(Status::atom(),State::any()) -> {ok,State::any()}.
on_cast(_Status,State) ->  {ok,State}.

-spec on_request(RequestData::term(),State::any()) -> {ok,ResponseData::binary(),State::any()}.
on_request(#http_data{request = #method.get}= HD,State) ->  get(HD,State);
on_request(#http_data{request = #method.post}= HD,State) ->  post(HD,State);
on_request(_,State) ->  bad_request(#http_response_code.service_unavailable,<<" This Service not Support ">>,State).

-spec on_response(RequestData::term(),State::any()) -> {reply,Reply::term(),State::any()}.
on_response(_RequestData,State) -> {reply,unhandle,State}.

-spec data_receiver(State::any()) -> http | ws |default.
data_receiver(_) -> http.

%%%===================================================================
%%% Internal methods
%%%===================================================================

get(#http_data{action = Action}= HData,#state{callbackmod = Mod,modstate = CState} = State) ->
  handle_request_resp(Mod:get(parse_action_params(Action),CState),HData,State).

post(#http_data{action = Action} =HData,#state{callbackmod = Mod,modstate = CState} = State) ->
  handle_request_resp(Mod:post(Action,CState),HData,State).

headers(Args,#http_data{headers = Headers} = HData,#state{callbackmod = Mod,modstate = CState} = State) ->
  handle_headers_resp(Mod:headers(Args,Headers,CState),HData,State).

body(Args,#http_data{request = #method.get,body = <<>>} = HD,#state{} = State) ->  request(Args,HD,State);
body(Args,#http_data{content_type = #content_type.json,body = JSONData} = HD,#state{callbackmod = Mod,modstate = CState} = State) ->
  handle_body_data_resp(Mod:json(Args,JSONData,CState),HD,State);
body(Args,#http_data{content_type = #content_type.xml,body = XMLData} = HD,#state{callbackmod = Mod,modstate = CState} = State) ->
  handle_body_data_resp(Mod:xml(Args,XMLData,CState),HD,State);
body(Args,#http_data{content_type = _,body = Data} = HD,#state{callbackmod = Mod,modstate = CState} = State) ->
  handle_body_data_resp(Mod:raw(Args,Data,CState),HD,State).

request(Args,HD,#state{callbackmod = Mod,modstate = CState} = State) ->  handle_request(Mod:request(Args,CState),HD,State).

return(Code,Content,_HD,#state{callbackmod = Mod,modstate = CState} = State) ->
  handle_return_resp(Mod:return(Content,CState),http_data:set_code(Code,http_data:new_response()),State).

return_headers(CType,Content,HD,#state{callbackmod = Mod,modstate = CState} = State) ->
  Body = element(2,parserl:encode(CType,Content)),
  handle_headers_response(Mod:return_headers(CState),http_data:body(Body,http_data:set_content_type(CType,HD)),State).

bad_request(Code,ErrorMessage,State) ->
  build_http(http_data:body(ErrorMessage,http_data:set_content_type(#content_type.text,http_data:set_code(Code,http_data:new_response()))),State).

build_http(HD,State) -> response(parserl:encode(http,HD),State).

response({ok,Bin},State) -> io:format("Res ~p~n",[Bin]),{ok,Bin,State};
response({error,Reason},State) -> io:format("Error ~p~n",[Reason]),
  bad_request(#http_response_code.internal_server_error,<<" Response Formation have some problem">>,State).

%%%===================================================================
%%% Helper methods
%%%===================================================================

handle_request_resp({continue,Args,CState},HData,State) -> headers(Args,HData,uCBs(State,CState));
handle_request_resp({return,Code,RetrunContent,CState},HD,State) -> return(Code,RetrunContent,HD,uCBs(State,CState));
handle_request_resp(_,_,_) -> throw({error,<<"invalid return">>}).

handle_headers_resp({continue,Args,CState},HData,State) -> body(Args,HData,uCBs(State,CState));
handle_headers_resp({return,Code,ReturnContent,CState},HD,State) -> return(Code,ReturnContent,HD,uCBs(State,CState));
handle_headers_resp(_,_,_) -> throw({error,<<"invalid return">>}).

handle_body_data_resp({continue,Request,CState},HD,State) -> request(Request,HD,uCBs(State,CState));
handle_body_data_resp({return,Code,ReturnContent,CState},HD,State) -> return(Code,ReturnContent,HD,uCBs(State,CState));
handle_body_data_resp(_,_,_) -> throw({error,<<"invalid return">>}).

handle_request({return,Code,ReturnContent,CState},HD,State) -> return(Code,ReturnContent,HD,uCBs(State,CState));
handle_request(_,_,_) -> throw({error,<<"invalid return">>}).

handle_return_resp({ok,ContentType,Content,CState},HD,State) -> return_headers(ContentType,data_encode(ContentType,Content),HD,uCBs(State,CState));
handle_return_resp(_,_,_) -> throw({error,<<"invalid return">>}).

handle_headers_response({ok,Headers,CState},HD,State) -> build_http(http_data:headers(Headers,HD),uCBs(State,CState));
handle_headers_response(_,_,_) -> throw({error,<<"invalid return">>}).

data_encode(#content_type.json,JSON) -> handle_data_encode(json_encoder:build(JSON));
data_encode(#content_type.xml,XML) -> handle_data_encode(xml_encoder:build(XML));
data_encode(#content_type.text,TEXT) -> TEXT;
data_encode(#content_type.html,HTML) -> handle_data_encode(html_encoder:build(HTML)).

handle_data_encode({ok,DataBin}) -> DataBin;
handle_data_encode({wrong_data,Reason}) -> throw({error,<<"Wrong Data to Encode ">>,Reason}).

%%%===================================================================
%%% Helper methods
%%%===================================================================

parse_action_params(Action) -> parse_action_params(Action,<<>>).
parse_action_params(<<>>,{Action,[]}) -> {Action,[]};
parse_action_params(<<>>,{Action,Agrs}) -> {Action,lists:reverse(Agrs)};
parse_action_params(<<>>,{Action,[<<Key/binary>>],<<Val/binary>>}) -> {Action,[{Key,Val}]};
parse_action_params(<<>>,{Action,[],<<Key/binary>>}) -> {Action,Key};
parse_action_params(<<>>,{Action,Args,<<Key/binary>>}) -> {Action,[Key|Args]};
parse_action_params(<<>>,<<Action/binary>>) -> {Action,[]};
parse_action_params(<<$?,Rest/binary>>,<<Action/binary>>) -> parse_action_params(Rest, {Action,[],<<>>});
parse_action_params(<<$=,Rest/binary>>,{Action,Sets,Key}) -> parse_action_params(Rest, {Action,[Key|Sets],<<>>});
parse_action_params(<<$,,Rest/binary>>,{Action,[Key|Sets],Val}) -> parse_action_params(Rest, {Action,[{Key,Val}|Sets],<<>>});
parse_action_params(<<H,Rest/binary>>,<<Action/binary>>) -> parse_action_params(Rest, <<Action/binary,H>>);
parse_action_params(<<H,Rest/binary>>,{Action,Sets,Acc}) -> parse_action_params(Rest, {Action,Sets,<<Acc/binary,H>>}).

uCBs(State,CState) -> State#state{modstate = CState}.



