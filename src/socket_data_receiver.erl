%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. Apr 2016 6:38 PM
%%%-------------------------------------------------------------------
-module(socket_data_receiver).
-author("tringapps").

%% API
-export([catch_data/2,catch_data/3]).

%%%===================================================================
%%% APIs
%%%===================================================================

catch_data(DataType,Socket) -> catch_data(DataType,Socket,5000).

catch_data(DataType,{gen_tcp,Socket},Timeout)when is_number(Timeout) -> receiver({gen_tcp,Socket,Timeout},{DataType,[]});
catch_data(DataType,{ssl,Socket},Timeout)when is_number(Timeout) -> receiver({ssl,Socket,Timeout},{DataType,[]});
catch_data(DataType,Socket,Timeout)when is_port(Socket) -> receiver({gen_tcp,Socket,Timeout},{DataType,[]});
catch_data(_,_,_Timeout) -> {wrong_data,<<"">>}.

%%%===================================================================
%%% Helper Functions
%%%===================================================================

receiver({gen_tcp,Socket,Timeout}=Soc,DataSet) ->  handle_tcp_recv(gen_tcp:recv(Socket,0,Timeout),DataSet,Soc);
receiver({ssl,Socket,Timeout}=Soc,DataSet) ->  handle_tcp_recv(ssl:recv(Socket,0,Timeout),DataSet,Soc).

handle_tcp_recv({ok,Data},{DataType,ParserState},Socket) -> handle_http_parser_res(parserl:decode_stream(DataType,Data,ParserState),DataType,Socket);
handle_tcp_recv({error,timeout},{_DataType,[]},_) ->  {error,timeout};
handle_tcp_recv({error,timeout},ParserState,Socket) ->  receiver(Socket,ParserState);
handle_tcp_recv({error,closed},_,_) -> {error,closed};
handle_tcp_recv({error,einval},_,_) -> {error,einval};
handle_tcp_recv(_,_,_) -> {error,unknown_error_in_tcp_receiver}.

handle_http_parser_res({incomplete,ParserState},DataType,Socket) -> receiver(Socket,{DataType,ParserState});
handle_http_parser_res({complete,Result},_,_) -> {ok,Result};
handle_http_parser_res({wrong_data,Reason},_,_) -> {error,Reason}.





